window.addEventListener('DOMContentLoaded', function(){
           // get the canvas DOM element
           var canvas = document.getElementById('renderCanvas');

           // load the 3D engine
           var engine = new BABYLON.Engine(canvas, true);

           // createScene function that creates and return the scene
           var createScene = function(){
               // create a basic BJS Scene object
               var scene = new BABYLON.Scene(engine);

// Creates, angles, distances and targets the camera
var camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 4, -2), scene);
   camera.attachControl(canvas, false);

               // create a basic light, aiming 0,1,0 - meaning, to the sky
               var light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0,1,0), scene);

               // create a built-in "sphere" shape; its constructor takes 6 params: name, segment, diameter, scene, updatable, sideOrientation
               var sphere = BABYLON.Mesh.CreateSphere('sphere1', 16, 2, scene);

               // move the sphere upward 1/2 of its height
               sphere.position.y = 1;

               // create a built-in "ground" shape;
               var ground = BABYLON.Mesh.CreateGround('ground1', 60, 60, 2, scene);

            // Keyboard events
   var inputMap ={};
   scene.actionManager = new BABYLON.ActionManager(scene);
   scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, function (evt) {
       inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
   }));
   scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, function (evt) {
       inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
   }));

   // Game/Render loop
   scene.onBeforeRenderObservable.add(()=>{
       if(inputMap["w"] || inputMap["ArrowUp"]){
           sphere.position.z+=0.1;
           camera.position.z+=0.1
       }
       if(inputMap["a"] || inputMap["ArrowLeft"]){
           sphere.position.x-=0.1;
           camera.position.x-=0.1
       }
       if(inputMap["s"] || inputMap["ArrowDown"]){
           sphere.position.z-=0.1;
           camera.position.z-=0.1
       }
       if(inputMap["d"] || inputMap["ArrowRight"]){
           sphere.position.x+=0.1;
           camera.position.x+=0.1
       }
   })

               // return the created scene
               return scene;
           }

           // call the createScene function
           var scene = createScene();

           // run the render loop
           engine.runRenderLoop(function(){
               scene.render();
           });

           // the canvas/window resize event handler
           window.addEventListener('resize', function(){
               engine.resize();
           });
       });
